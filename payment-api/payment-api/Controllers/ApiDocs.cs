﻿using Microsoft.AspNetCore.Mvc;

namespace payment_api.Controllers {

    [ApiController]
    [Route("api-docs")]
    public class ApiDocs : ControllerBase{
        [HttpGet]
        public IActionResult Index() {
            return Redirect("swagger/index.html");
        }
    }
}
