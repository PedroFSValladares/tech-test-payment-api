﻿using Microsoft.AspNetCore.Mvc;
using payment_api.Models;

namespace payment_api.Controllers {
    [ApiController]
    [Route("Venda")]
    public class VendaController : ControllerBase{

        public VendaController() {
            
        }

        [HttpPost("Criar")]
        public IActionResult Criar(Venda venda) { 
            if(!ModelState.IsValid) {
                return UnprocessableEntity();
            }
            venda.Status = VendaStatus.Aguardando_Pagamento;
            venda.Data = DateTime.Now;
            
            VendaDB.Add(venda);

            return Ok(venda);
        }

        [HttpGet]
        public IActionResult ObterPorId(int id) {
            var venda = VendaDB.Find(id);
            return venda == null ? NotFound() : Ok(venda);
        }

        [HttpPost("Atualizar")]
        public IActionResult AtualizarVenda(int id, VendaStatus novoStatus) {
            var venda = VendaDB.Find(id);

            switch(novoStatus){
                case VendaStatus.Pagamento_Aprovado:
                    if(venda.Status != VendaStatus.Aguardando_Pagamento) {
                        return BadRequest();
                    }
                    break;
                case VendaStatus.Enviado_a_transportadora:
                
                    if(venda.Status != VendaStatus.Pagamento_Aprovado) {
                        return BadRequest();
                    }
                    break;
                case VendaStatus.Entregue:
                    if(venda.Status != VendaStatus.Enviado_a_transportadora) {
                        return BadRequest();
                    }
                    break;
                case VendaStatus.Cancelada:
                    if(venda.Status != VendaStatus.Aguardando_Pagamento && venda.Status != VendaStatus.Pagamento_Aprovado) {
                        return BadRequest();
                    }
                    break;
            }

            venda.Status = novoStatus;
            var newVenda = VendaDB.Update(venda);
            return Ok(newVenda)
;        }
    }
}
