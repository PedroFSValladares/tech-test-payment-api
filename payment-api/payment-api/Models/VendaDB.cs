﻿namespace payment_api.Models {
    public static class VendaDB {
        private static int id = 0;
        private static Dictionary<int, Venda> vendas = new();

        public static void Add(Venda venda) {
            vendas.Add(id, venda);
            Console.WriteLine(id);
            id += 1;
        }

        public static Venda Find(int id) {
            vendas.TryGetValue(id, out Venda venda);
            return venda;
        }

        public static void Delete(int id) {
            vendas.Remove(id);
        }

        public static Venda Update(Venda venda) {
            vendas.TryGetValue(venda.Id, out Venda vendaDB);
            vendaDB.Vendedor = venda.Vendedor;
            vendaDB.Data = venda.Data;
            vendaDB.Status = venda.Status;
            vendaDB.Itens = venda.Itens;

            return vendaDB;
        }
    }
}
