﻿namespace payment_api.Models {
    public enum VendaStatus {
        Aguardando_Pagamento,
        Pagamento_Aprovado,
        Enviado_a_transportadora,
        Entregue,
        Cancelada
    }
}
